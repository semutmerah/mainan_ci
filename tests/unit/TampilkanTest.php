<?php
require_once("index.php");
use PHPUnit\Framework\TestCase;

class TampilkanTest extends TestCase {
    public function testIsKabarkanCorrect() {
        $expected = 'Apapun';
        $this->expectOutputString($expected);
        $tampilkan = new Tampilkan();
        $result = $tampilkan->kabarkan('Apapun');
    }
}
?>